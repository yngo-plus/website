import {h, Fragment} from 'preact'
import {Link} from 'preact-router/match'
import Icon from 'preact-material-components/Icon'
import 'preact-material-components/Icon/style.css'

import '../styles/Footer.scss'

export const Footer = () => {
  return (
    <>
      <footer>
        <div className='footer-license'>
          Licensed under{' '}
          <a
            target='_blank'
            rel='noopener noreferrer'
            href='https://opensource.org/licenses/MIT'
          >
            MIT
          </a>
        </div>
        <div className='footer-icons'>
          <a href='mailto:yngoplus@gmail.com'>
            <Icon>email</Icon>
          </a>
          <a href='https://gitlab.com/yngo-plus'>
            <Icon>code</Icon>
          </a>
          <a href='https://www.instagram.com/yngoplus'>
            <Icon>tag</Icon>
          </a>
          <a href='https://t.me/yngoplus'>
            <Icon>send</Icon>
          </a>
          <a href='https://www.facebook.com/yngoplus'>
            <Icon>facebook</Icon>
          </a>
          <a href='https://www.youtube.com/channel/UC6u8kYYLTxYBY8739xOeo5g'>
            <Icon>smart_display</Icon>
          </a>
        </div>
        <Link href='/legal/'>
          <div className='footer-copyright'>
            © 2021 YNGO PLUS. All rights reversed.
          </div>
        </Link>
      </footer>
    </>
  )
}

export default Footer
