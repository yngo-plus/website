import {h, Fragment, FunctionComponent} from 'preact'
import {Button, ButtonColor, ButtonSize, ButtonStyle} from './Button'

import '../styles/HeroSection.scss'

export interface Props {
  lightBg: boolean
  topLine: string
  topLink: string
  lightText: boolean
  lightTextDesc: boolean
  headline: string
  description: string
  buttonLabel: string
  buttonLink: string
  isLeftSided?: boolean
  src: string
  alt: string
  sectionID: string
  shouldBeFullscreen?: boolean
}

export const HeroSection: FunctionComponent<Props> = ({
  lightBg,
  topLine,
  topLink,
  lightText,
  lightTextDesc,
  headline,
  description,
  buttonLabel,
  buttonLink,
  isLeftSided = false,
  src,
  alt,
  sectionID,
  shouldBeFullscreen = false,
}) => {
  return (
    <>
      <section id={sectionID}>
        <div
          className={
            (lightBg
              ? 'home__hero-section'
              : 'home__hero-section dark-background') +
            (shouldBeFullscreen ? ' fullscreen' : '')
          }
        >
          <div className='container'>
            <div
              className='row home__hero-row'
              style={{
                display: 'flex',
                flexDirection: isLeftSided === true ? 'row-reverse' : 'row',
              }}
            >
              <div className='col'>
                <div className='col__text-wrapper'>
                  <a
                    className='text-wrapper__top'
                    target='_blank'
                    rel='noopener noreferrer'
                    href={topLink}
                  >
                    {topLine}
                  </a>
                  <h1
                    className={
                      lightText
                        ? 'text-wrapper__heading'
                        : 'text-wrapper__heading dark-content'
                    }
                  >
                    {headline}
                  </h1>
                  <p
                    className={
                      lightTextDesc
                        ? 'text-wrapper__subtitle'
                        : 'text-wrapper__subtitle dark-content'
                    }
                  >
                    {description}
                  </p>
                  <a href={buttonLink}>
                    <Button
                      buttonStyle={ButtonStyle.Primary}
                      buttonSize={ButtonSize.Wide}
                      buttonColor={ButtonColor.Primary}
                    >
                      {buttonLabel}
                    </Button>
                  </a>
                </div>
              </div>
              <div className='col'>
                <div className='col__image-wrapper'>
                  <img src={src} alt={alt} className='image-wrapper__content' />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
