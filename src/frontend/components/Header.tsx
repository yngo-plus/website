import {h} from 'preact'
import {Link} from 'preact-router/match'

// @ts-ignore
import logo from 'url:../images/logo.png'
import '../styles/Header.scss'

export const Header = () => {
  function moveTo(id: string) {
    const element: HTMLElement = document.getElementById(id)!
    element.scrollIntoView({behavior: 'smooth'})
  }

  return (
    <header>
      <Link className='logo' href='/home/'>
        <img src={logo} alt='logo'></img>
      </Link>
      <nav>
        <Link
          href='/home/#intro'
          className='header-top__navbar-item'
          onClick={() => moveTo('intro')}
        >
          Хто ми
        </Link>
        <Link
          href='/home/#goal'
          className='header-top__navbar-item'
          onClick={() => moveTo('goal')}
        >
          Наша місія
        </Link>
        <Link
          href='/home/#sponsorship'
          className='header-top__navbar-item'
          onClick={() => moveTo('sponsorship')}
        >
          Спонсорство
        </Link>
        <Link
          href='/home/#team'
          className='header-top__navbar-item'
          onClick={() => moveTo('team')}
        >
          Команда
        </Link>
        <Link
          href='/home/#contact'
          className='header-top__navbar-item'
          onClick={() => moveTo('contact')}
        >
          Контакти
        </Link>
      </nav>
    </header>
  )
}

export default Header
