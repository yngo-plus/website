import {h} from 'preact'
import {
  Button,
  ButtonColor,
  ButtonSize,
  ButtonStyle,
} from '../components/Button'
import Icon from 'preact-material-components/Icon'
import {googleApplyFormLink, makeInvestmentLink} from '../pages/Data/Constants'

import '../styles/Card.scss'

export const Card = () => {
  return (
    <section id='goal' className='card__section'>
      <div className='card__wrapper'>
        <h1 className='card__heading'>Наша місія</h1>
        <div className='card__container'>
          <a href={googleApplyFormLink} className='card__link'>
            <div className='card__info'>
              <div className='card__icon'>
                <Icon>groups</Icon>
              </div>
              <h2>Для кожного</h2>
              <ul className='card__features'>
                <li>
                  Вебінари доступні всім українцям – незалежно від місця
                  проживання, гендеру, політичних та релігійних переконань.
                </li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Primary}
              >
                Долучитися до проєкту
              </Button>
            </div>
          </a>
          <a href={makeInvestmentLink} className='card__link'>
            <div className='card__info'>
              <div className='card__icon'>
                <Icon>school</Icon>
              </div>
              <h2>Неформально</h2>
              <ul className='card__features'>
                <li>
                  Ми не хочемо асоціюватися з гімназією, тому замість томливих
                  вчителів запрошуємо креативних та досвідчених спікерів.
                </li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.White}
              >
                Підтримати
              </Button>
            </div>
          </a>
          <a href={googleApplyFormLink} className='card__link'>
            <div className='card__info'>
              <div className='card__icon'>
                <Icon>savings</Icon>
              </div>
              <h2>Безкоштовно</h2>
              <ul className='card__features'>
                <li>
                  Матеріальне становище не має впливати на доступ до
                  інформаційних джерел.
                </li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.White}
              >
                Підтримати
              </Button>
            </div>
          </a>
          <a href={makeInvestmentLink} className='card__link'>
            <div className='card__info'>
              <div className='card__icon'>
                <Icon>face</Icon>
              </div>
              <h2>Від молоді - для молоді</h2>
              <ul className='card__features'>
                <li>Лише молодь знає, що їй насправді потрібно.</li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Primary}
              >
                Долучитися до проєкту
              </Button>
            </div>
          </a>
        </div>
      </div>
    </section>
  )
}

export default Card
