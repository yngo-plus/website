import {h, Fragment, FunctionalComponent} from 'preact'
import {Link} from 'preact-router'
import {useState} from 'preact/hooks'
import Icon from 'preact-material-components/Icon'
import 'preact-material-components/Icon/style.css'
import {Button, ButtonColor, ButtonSize, ButtonStyle} from './Button'
import {SliderData} from '../pages/Data/SliderData'

import '../styles/ImageSlider.scss'

export interface Props {
  slides: Array<{image: string; name: string; role: string}>
}

export const ImageSlider: FunctionalComponent<Props> = ({slides}) => {
  const [current, setCurrent] = useState(0)
  const length = slides.length

  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1)
  }

  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current - 1)
  }

  if (!Array.isArray(slides) || slides.length <= 0) {
    return null
  }

  return (
    <section id='team' className='slider'>
      <h1 className='team__heading'>Команда</h1>
      <div className='team__slider'>
        <Icon className='left-arrow' onClick={prevSlide}>
          arrow_back
        </Icon>
        {SliderData.map((slide, index) => {
          return (
            <div
              className={index === current ? 'slide active' : 'slide'}
              key={index}
            >
              {index === current && (
                <>
                  <img src={slide.image} alt='Член команди' className='image' />
                  <p className='team__name'>{slide.name}</p>
                  <p className='team__role'>{slide.role}</p>
                </>
              )}
            </div>
          )
        })}
        <Icon className='right-arrow' onClick={nextSlide}>
          arrow_forward
        </Icon>
      </div>
      <div className='team__click-more'>
        <Link href='/team/'>
          <Button
            buttonStyle={ButtonStyle.Outline}
            buttonSize={ButtonSize.Wide}
            buttonColor={ButtonColor.Black}
          >
            Дізнатися більше!
          </Button>
        </Link>
      </div>
    </section>
  )
}

export default ImageSlider
