import {h} from 'preact'
import {useState} from 'preact/hooks'
import Icon from 'preact-material-components/Icon'
import 'preact-material-components/Icon/style.css'

import '../styles/ScrollUp.scss'

const ScrollUp = () => {
  const [visible, setVisible] = useState(false)

  const toggleVisibility = () => {
    const scrolled = document.documentElement.scrollTop
    if (scrolled >= 666) {
      setVisible(true)
    } else {
      setVisible(false)
    }
  }

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  window.addEventListener('scroll', toggleVisibility)

  return (
    <div className='smooth'>
      <Icon
        onClick={scrollToTop}
        style={{display: visible ? 'inline' : 'none'}}
      >
        upgrade
      </Icon>
    </div>
  )
}

export default ScrollUp
