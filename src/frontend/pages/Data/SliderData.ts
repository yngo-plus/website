// @ts-ignore
import alina from 'url:../../images/slider-assembly/alina.png'
// @ts-ignore
import lillia from 'url:../../images/slider-assembly/lillia.png'
// @ts-ignore
import maria from 'url:../../images/slider-assembly/maria.png'
// @ts-ignore
import marianna from 'url:../../images/slider-assembly/marianna.png'
// @ts-ignore
import max from 'url:../../images/slider-assembly/max.png'
// @ts-ignore
import nathan from 'url:../../images/slider-assembly/nathan.png'
// @ts-ignore
import roman from 'url:../../images/slider-assembly/roman.png'
// @ts-ignore
import sasha from 'url:../../images/slider-assembly/sasha.png'
// @ts-ignore
import vladislav from 'url:../../images/slider-assembly/vladislav.png'

export const SliderData: Array<{image: string; name: string; role: string}> = [
  {
    image: alina,
    name: 'Аліна Нямцу',
    role: 'Секретарка',
  },
  {
    image: lillia,
    name: 'Лілія Алєксанова',
    role: 'HR-менеджерка',
  },
  {
    image: maria,
    name: 'Марія Рєвіна',
    role: 'Менеджерка онлайн-проєктів',
  },
  {
    image: marianna,
    name: 'Маріанна Іовенко',
    role: 'Президентка',
  },
  {
    image: max,
    name: 'Максим Куліков',
    role: 'Контент-менеджер',
  },
  {
    image: nathan,
    name: 'Нейтан Сиваш',
    role: 'Фінансовий директор',
  },
  {
    image: roman,
    name: 'Роман Зубрицький',
    role: 'Менеджер офлайн-проєктів',
  },
  {
    image: sasha,
    name: 'Олександра Вісковатова',
    role: 'IT-менеджерка',
  },
  {
    image: vladislav,
    name: 'Владислав Бурко',
    role: 'Дизайнер',
  },
]
