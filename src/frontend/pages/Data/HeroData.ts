import {Props} from '../../components/HeroSection'
import {googleApplyFormLink, makeInvestmentLink} from './Constants'

// @ts-ignore
import firstDoodle from 'url:../../images/doodle_2.png'
// @ts-ignore
import midDoodle from 'url:../../images/doodle_1.png'
// @ts-ignore
import lastDoodle from 'url:../../images/doodle_2.png'

export const homeLayoutObjOne: Props = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'Донат',
  topLink: googleApplyFormLink,
  headline: 'YNGO PLUS',
  description:
    'Ми створюємо безкоштовні можливості неформальної освіти для української молоді та не отримуємо за це прибутку. За плечима нашої організації численні вебінари та курси на теми, які не обговорюють в школах та університетах. Ми навчаємо успішно проходити інтерв`ю, зніматися в кіно та розумітися на фінансовій грамотності.',
  buttonLabel: 'Приєднатися до команди',
  buttonLink: makeInvestmentLink,
  isLeftSided: false,
  src: firstDoodle,
  alt: 'Ми - це ..',
  sectionID: 'intro',
  shouldBeFullscreen: true,
}

export const homeLayoutObjTwo: Props = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'вай-ен-джі-о пе-ел-ю-ес',
  topLink: googleApplyFormLink,
  headline: 'Спонсорство',
  description:
    'YNGO PLUS - неприбуткова організація, яка надає українській молоді безкоштовні навчальні ресурси. Ми організовуємо вебінари та онлайн курси на численні теми - від написання ідеального резюме до робототехніки в Україні. Наша команда виграла грант від InScience та UNICEF на створення настільної гри, спрямованої на розвиток критичного мислення та навичок розв язання критичних ситуацій, і на проведення різних офлайн івентів потрібні відповідні кошти.',
  buttonLabel: 'Допомогти організації...',
  buttonLink: googleApplyFormLink,
  src: midDoodle,
  alt: 'Інвестиція у майбутнє України',
  sectionID: 'sponsorship',
}

export const homeLayoutObjAfterThree: Props = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'Вступити',
  topLink: googleApplyFormLink,
  headline: 'Контакти',
  description:
    'Пишіть нам на пошту: yngoplus@gmail.com. Ми також є в усіх соціальних мережах: від фейсбуку та інстаграму до телеграму з ютюбом!',
  buttonLabel: 'Стати волонтером',
  buttonLink: googleApplyFormLink,
  isLeftSided: true,
  src: lastDoodle,
  alt: 'Месенджери та інший там інтернет...',
  sectionID: 'contact',
}
