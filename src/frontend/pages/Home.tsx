import {h, Fragment} from 'preact'
import {HeroSection} from '../components/HeroSection'
import Card from '../components/Card'
import {
  homeLayoutObjOne,
  homeLayoutObjTwo,
  homeLayoutObjAfterThree,
} from './Data/HeroData'
import ImageSlider from '../components/ImageSlider'
import {SliderData} from '../pages/Data/SliderData'

export default () => (
  <>
    <HeroSection {...homeLayoutObjOne} />
    <Card />
    <HeroSection {...homeLayoutObjTwo} />
    <ImageSlider slides={SliderData} />
    <HeroSection {...homeLayoutObjAfterThree} />
  </>
)
