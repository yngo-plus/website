import {h, FunctionComponent} from 'preact'
import Router, {Route} from 'preact-router'
import Redirect from '../components/Redirect'
import Header from '../components/Header'
import Footer from '../components/Footer'
import ScrollUp from '../components/ScrollUp'
import Home from './Home'

import '../styles/App.scss'

export const App: FunctionComponent = () => (
  <div id='app'>
    <Header />
    <main>
      <Router>
        <Route path='/home/' component={Home} />
        {/*
        // @ts-ignore */}
        <Redirect path='/' to='/home/' />
      </Router>
    </main>
    <Footer />
    <ScrollUp />
  </div>
)

export default App
